import './floodfill'
import suggest from './suggest'

// Connect to websocket
const socket = io('https://skribbl.io')

// Constant

const words = [
    "sachet",
    "hôtel",
    "assassin",
    "Dracaufeu",
    "magma",
    "poivron",
    "pâtisserie",
    "ourson en peluche",
    "lampe à lave",
    "sous-marin",
    "avocat",
    "voisinage",
    "pied de biche",
    "serrure",
    "tour",
    "casserole",
    "jardin",
    "McDonalds",
    "printemps",
    "pancake",
    "Schtroumpf",
    "boulette de viande",
    "glace à l'eau",
    "pouvoir",
    "consience",
    "courage"
]

const prenom = [
    "Adam",
    "Alex",
    "Alexandre",
    "Alexis",
    "Anthony",
    "Antoine",
    "Benjamin",
    "Cédric",
    "Charles",
    "Christopher",
    "David",
    "Dylan",
    "Édouard",
    "Elliot",
    "Émile",
    "Étienne",
    "Félix",
    "Gabriel",
    "Guillaume",
    "Hugo",
    "Isaac",
    "Jacob",
    "Jérémy",
    "Jonathan",
    "Julien",
    "Justin",
    "Léo",
    "Logan",
    "Loïc",
    "Louis",
    "Lucas",
    "Ludovic",
    "Malik",
    "Mathieu",
    "Mathis",
    "Maxime",
    "Michaël",
    "Nathan",
    "Nicolas",
    "Noah",
    "Olivier",
    "Philippe",
    "Raphaël",
    "Samuel",
    "Simon",
    "Thomas",
    "Tommy",
    "Tristan",
    "Victor",
    "Vincent",
]

const hello = [
    "Yo",
    "yo",
    "slt",
    "Slt",
    "Salut",
    "salut"
]

// Game variables

let players

// Left pannel

function debug() {
    const debugColumn = document.querySelector('.debug')
    // Debug
    const args = Array.from(arguments).join(' ')
    console.log(args)
    const log = document.createElement('div')
    log.appendChild(document.createTextNode(args))
    debugColumn.appendChild(log)
    // Scroll to the bottom of the debug column
    debugColumn.scrollTo(0, debugColumn.scrollHeight)
}

function updateSuggestions(words) {
    let suggestions = document.querySelector('.word-suggestions')
    suggestions.innerHTML = ''
    for (const word of words) {
        const suggestion = document.createElement('span')
        suggestion.appendChild(document.createTextNode(word))
        suggestion.className = 'word-suggestions--word'
        suggestion.addEventListener('click', e => socket.emit('chat', e.target.innerHTML))
        suggestions.appendChild(suggestion)
        // Scroll to the bottom of the column
        suggestions.scrollTo(0, suggestions.scrollHeight)
    }
}


// Canvas

let colors = ["#FFF", "#000", "#C1C1C1", "#4C4C4C", "#EF130B", "#740B07", "#FF7100", "#C23800", "#FFE400", "#E8A200", "#00CC00", "#005510", "#00B2FF", "#00569E", "#231FD3", "#0E0865", "#A300BA", "#550069", "#D37CAA", "#A75574", "#A0522D", "#63300D"]

let canvas = document.getElementById("myCanvas")
let ctx = canvas.getContext("2d");

function clearCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function drawCommands(commands) {
    for (const command of commands) {
        // Pencil
        if (command[0] == 0) {
            // Params
            const color = colors[command[1]]
            const linewidth = command[2]
            const x1 = command[3]
            const y1 = command[4]
            const x2 = command[5]
            const y2 = command[6]
            // Draw on canvas
            ctx.beginPath()
            ctx.lineWidth = linewidth
            ctx.strokeStyle = color
            ctx.lineJoin = ctx.lineCap = 'round';
            // ctx.moveTo(x1, y1)
            // ctx.lineTo(x2, y2)
            ctx.quadraticCurveTo(x1, y1, x2, y2)
            ctx.stroke()
            ctx.closePath()
        }
        // Eraser
        else if (command[0] == 1) {
            const color = "#FFF"
            const linewidth = command[1]
            const x1 = command[2]
            const y1 = command[3]
            const x2 = command[4]
            const y2 = command[5]
            // Draw on canvasDraw
            ctx.beginPath()
            ctx.lineWidth = linewidth
            ctx.strokeStyle = color
            ctx.lineJoin = ctx.lineCap = 'round';
            // ctx.moveTo(x1, y1)
            // ctx.lineTo(x2, y2)
            ctx.quadraticCurveTo(x1, y1, x2, y2)
            ctx.stroke()
            ctx.closePath()
        }
        // Fill flood
        else if (command[0] == 2) {
            const color = colors[command[1]]
            const x = command[2]
            const y = command[3]
            ctx.fillStyle = color
            ctx.fillFlood(x, y, 32)
        }
    }
}

// Chat

let chatDisplay = document.querySelector('.chat-display')

function addMessage(username, message) {
    // Username
    const chatMessageName = document.createElement('span')
    chatMessageName.className = "chat-message--name"
    chatMessageName.appendChild(document.createTextNode(username))
    // Message
    const chatMessageMessage = document.createElement('span')
    chatMessageMessage.className = "chat-message--message"
    chatMessageMessage.appendChild(document.createTextNode(message))
    // Message node
    const chatMessage = document.createElement('div')
    chatMessage.className = "chat-message"
    chatMessage.appendChild(chatMessageName)
    chatMessage.appendChild(chatMessageMessage)
    // Append node
    chatDisplay.appendChild(chatMessage)
    // Scroll to the bottom of the chat
    chatDisplay.scrollTo(0, chatDisplay.scrollHeight)
}

function dispatchCommand(command) {
    debug('NEW COMMAND:', command)
}

document.getElementById('chat-form').addEventListener('submit', e => {
    // Prevent page reload
    e.preventDefault()
    // Message
    const message = e.target[0].value
    // Commands ?
    if (message.startsWith('!')) {
        dispatchCommand(message)
    }
    else {
        // Send the message
        socket.emit('chat', message)
    }
    // Clean input box
    e.target[0].value = ""
})

// Socket

socket.on('connect', function () {
    debug("Connected");

    const name = prenom[Math.floor(Math.random() * prenom.length)]
    debug("Je m'appelle", name);

    socket.emit("userData", {
        name: name,
        code: "",
        avatar: [11, 30, 11, -1],
        join: "skgVXxaIJu12CFgevHAL",
        language: "French",
        createPrivate: false
    })
})

socket.on("login", function () {
    debug("Logged");
})

socket.on("lobbyConnected", function (t) {
    debug("Players", t.players.length, t.players.map(e => e.name).join(', '));

    players = t.players

    drawCommands(t.drawCommands)

    // setTimeout(() => {
    //     socket.emit('chat', hello[Math.floor(Math.random() * hello.length)])
    // }, 1000)

    // clearInterval(intId)
    // intId = setInterval(() => {
    //     socket.emit('chat', words[Math.floor(Math.random() * words.length)])
    // }, 15000)
})

socket.on("kicked", function () {
    debug("Sorry Kicked");
})
// socket.on("drawCommands", function (t) {
//     debug("Draw", t);
// })
socket.on("chat", function (t) {
    const p = players.find(e => e.id === t.id)
    addMessage(p.name, t.message)
    if (t.message.toUpperCase().includes('BOT') && t.message != 'Je suis pas un bot...') {
        setTimeout(() => {
            socket.emit('chat', 'Je suis pas un bot...')
        }, 2500)
    }
    // debug("Chat", p.name, t);
})

socket.on("lobbyCurrentWord", function (t) {
    document.querySelector('.draw--guess-word').innerHTML = t
    suggest(t).then(w => {
        updateSuggestions(w)
    })
})

socket.on("lobbyReveal", function (t) {
    debug("Reveal", t.word);
    // fs.appendFile('words.txt', t.word + '\n', err => {
    //     if (err) console.error(err)
    // })
})

socket.on("lobbyChooseWord", function (t) {
    debug("Choose word", t);
    if (t.words) {
        debug(t.words.map(w => w + '\n').join(''))
        socket.emit("lobbyChooseWord", 0)
        // fs.appendFile('words.txt', t.words.map(w => w + '\n').join(''), err => {
        //     if (err) console.error(err)
        // })
        // socket.close();
    }
})

socket.on("lobbyState", function (t) {
    // debug("State", t);
})

socket.on("lobbyPlayerConnected", function (t) {
    debug("+ New player", t.name);
    players.push(t)
})

socket.on("drawCommands", t => drawCommands(t))

socket.on("canvasClear", clearCanvas)

socket.on("lobbyPlayerDisconnected", function (t) {
    const p = players.find(e => e.id === t)
    debug("- Player", p.name, "leave");
    players = players.filter(e => e.id !== t.id)
})

socket.on("lobbyPlayerGuessedWord", function (t) {
    const p = players.find(e => e.id === t);
    debug(p.name, "find the word");
})

socket.on("lobbyGuessClose", function (t) {
    // debug("Hint:", t);
})


socket.on('disconnect', function (reason) {
    debug("Disconnected", reason)
    if (reason === 'io server disconnect') {
        debug("Force reco in 10s")
        // the disconnection was initiated by the server, you need to reconnect manually
        setTimeout(() => socket.connect(), 10000)
    } else if (reason === 'io client disconnect') {
        debug("Client deco in 10s")
        setTimeout(() => socket.connect(), 10000)
    }
})

socket.on('error', err => console.error('Error::', err))

socket.on('reconnect_error', (error) => {
    debug("reco error", error);
})

socket.on('reconnecting', (attemptNumber) => {
    debug("reco", attemptNumber);
})
