# Enhanced skribbl.io

The project `Enhanced skribbl.io` aims to provide a web application to play in [skribbl.io](https://skribbl.io) room but with advanced features like word suggestion, advanced draw tools or autodraw.

## Use the app

## Build the app

## TODO

- Add the same draw tool that are in official skribbl.io
- Add advanced draw tools (line tool, shape tools (circle, rectangle, triange), text tool, slider to choose the brush/shape size)
- Add autodraw (the user has to choose the picture to draw among a selection of suggested pictures taked from Google image)
- Add players list
